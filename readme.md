# Quartic AI - Data Scientist - Puzzle



## The Conclusion

* See [final.csv](/final.csv) for prediction results. 

* See [02_final.ipynb](/02_final.ipynb) for jupyter notebook containing the code to generate that final result.



## Problem Statement

* You are asked to build the most accurate model you can to predict target column for data_test.csv. 
* The metric to reflect accuracy can be defined by yourself. 

* **Data Description**
  * 'id': id column for data_train, data_test, respectively
  * 'num*': numerical features
  * 'der*': derived features from other features
  * 'cat*': categorical features
  * 'target': target column, only exists in data_train. it is binary.

* There are potentially missing values in each column.
* The goal is to predict 'target' column for data_test.csv.
* **Artifacts Submission**
  * The solution should have a result csv file with two columns:
    1. 'id': the id column from data_test.csv
    2. 'target': the predicted probability of target being 1
  * The corresponding code to reproduce the result csv file should be included as well.



## Observations

1. The dataset is big enough with 6L and 9L rows for train and test respectively.

2. Dataset  is anonymized with clueless column names not allowing the data scientist to make inference of  the domain. On a brighter side, your personal bias is shut and you are saved from doing your homework in the domain to do any feature engineering :neutral_face:

3. Dataset is Imbalanced, in that the target label values are in the ratio = 0:1::96%:4%. It is therefore safef to assume that the target events (1s) are abnormal ones and finding it out is the  objective. <u>But in the absence of domain context and confirmed business objective, it is hard to decide the tradeoff that we may have to make in Precision versus Recall.</u>

  - How to split data?
    1. `scikit-learn` uses Stratified-Split by default and that helps!
    2. Can also use skcontrib's `imblearn` to balance the dataset and see if it improves prediction performance.

4. Of the 56 cols (excl the 'id', and 'target' cols), the distribution of its types are like below:

  * numeric cols = 23
  * derived cols = 19
  * categorical cols = 14

5. Columns with lots of missing values (in both train/test dataset): cat6, cat8, num18, num22, (cat10, cat3 as well??) 
  How to deal with these columns???

  Columns cat6, cat8 having significant NaN values
  Columns cat6, cat8, cat10 have binary values : 0/1
  Column cat3 has many category values with mode value being 0, having a significant  count of 5L+ in train dataset.

6. From the correlation matrix / heatmap, apparently the derived features don't bear any correlation with the  dependent feature. The derived features are all noise??? and so these columns can be ignored, I guess.



## Q & A

Please provide short answers to the following discussion questions:

1. Briefly describe the **conceptual approach** you chose! What are the **trade-offs**?
   - **Conceptual Approach**
     - Make EDA/observations (see section above) on the dataset for insight.
     - Do quick base-lining of performance with RandomForest
     - Draw performance metrics with scikit-learn's default parameters to see how the varied models perform.
     - Pick a model that is relatively less complex and can perform faster in computation time taking less resources, if the core metrics performance for all being more/less equal. I chose `LGBMClassifier` going by this approach.
   - **Trade-offs**
     - Imbalanced dataset (See point 3 in Observation section above) brings with it its own set of challenges and issues that mandates trade-offs. In this case, without any explilcit objective, it is hard to determine the importance of Precision over Recall (or vice-versa). I tried finding a middle ground with a little compromise on the f1-score.
     - I chose less complex, fast computing and less resource intensive model. This compromises a bit on the f1score and guess that is better to begin with at least.
2. What's the model **performance**? What is the **complexity**? Where are the **bottlenecks**?
   - Use f1-score as metric for model performance. But we are still left with the challenge of deciding the trade-off between Precision and Recall.
   - <u>The absence of business context and goals are the real bottlenecks. When we don't really know where  we are heading, it doesn't matter which road we take so long as it is not suicidal, isn't it?</u>
3. If you had more time, what **improvements** would you make, and in what order of **priority**?
   * I tried most of the possibilities to improve the chosen model performance.
   * Given more time, I'll perhaps try to play  more on feature engineering (creating new features and dropping existing features) to see if it improves the score.
   * Given more time and possibly opportunity to interact with the stakeholders, I'd seek to know about the business context, in order to attempt making informed decisions.
   * Make the final_code better by pushing preprocessing to pipelines, for the sake of brevity, elegance, maintainability and readability. [A High Priority to do this in regular interval]



## Assessment Criteria

In no specific order:

- If your solution satisfies the requirements
- How the code and functionality is tested
- The understandability and maintainability of your code
- The cleanliness of design and implementation
- Time performance on a standard laptop
- Answers to the discussion questions.